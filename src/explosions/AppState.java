package explosions;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;

/**
 *
 * @author Soverliss
 */
public abstract class AppState extends AbstractAppState {

    protected Node rootNode;
    protected SimpleApplication app;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
        this.app.getRootNode().attachChild(rootNode = new Node());
    }

    protected abstract void initNetwork();

}
