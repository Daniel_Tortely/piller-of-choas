package explosions;

/**
 *
 * @author Soverliss
 */
public interface Broadcaster {

    void send(NetworkMessage networkMessage);
    void send(NetworkMessage networkMessage, int clientId);
}
