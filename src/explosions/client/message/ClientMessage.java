/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package explosions.client.message;

import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import explosions.NetworkMessage;

/**
 *
 * @author Soverliss
 */
public abstract class ClientMessage extends NetworkMessage{
    
    @Serializable
    public static class ChatMessage extends ClientMessage {

        public String message;

        public ChatMessage() {
        }

        public ChatMessage(String message) {
            this.message = message;
        }
    }

    @Serializable
    public static class PlayerInputMessage extends ClientMessage {

        public int id;
        public String command;
        public boolean pressed;
        public Vector3f lookDirection;

        public PlayerInputMessage() {
        }

        public PlayerInputMessage(int id, String command, boolean pressed) {
            this.id = id;
            this.command = command;
            this.pressed = pressed;
            lookDirection = Vector3f.ZERO;
        }

        public PlayerInputMessage(int id, String command, Vector3f lookDirection) {
            this(id, command, true);
            if (lookDirection == null) {
                lookDirection = Vector3f.ZERO;
            }
            this.lookDirection = lookDirection;
        }
    }

    @Serializable
    public static class PlayerLookMessage extends ClientMessage {

        public int id;
        public Vector3f lookDirection;

        public PlayerLookMessage() {
        }

        public PlayerLookMessage(int id, Vector3f lookDirection) {
            this.id = id;
            this.lookDirection = lookDirection;
        }
    }
}
