package explosions.client;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author Soverliss
 */
public class HUDScreen implements ScreenController {

    private Label health;
    private Label mana;
    private Label enemies;
    public static final String HEALTH = "healthValue";
    public static final String MANA = "manaValue";
    public static final String ENEMIES = "enemiesValue";

    @Override
    public void bind(Nifty nifty, Screen screen) {
        health = screen.findNiftyControl(HEALTH, Label.class);
        mana = screen.findNiftyControl(MANA, Label.class);
        enemies = screen.findNiftyControl(ENEMIES, Label.class);

    }

    public void update(String labelId, String info) {
        switch (labelId) {
            case HEALTH:
                health.setText(info);
            break;
            case MANA:
                mana.setText(info);
            break;
            case ENEMIES:
                enemies.setText(info);
            break;
        }
    }

    @Override
    public void onStartScreen() {
    }

    @Override
    public void onEndScreen() {
    }
}
