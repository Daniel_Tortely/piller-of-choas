package explosions.client.entity;

import com.jme3.input.controls.ActionListener;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import static explosions.Factory.ID;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import explosions.Broadcaster;

/**
 *
 * @author Soverliss
 */
public class PlayerControl extends UpdateControl implements ActionListener {

    public static final String JUMP = "Jump";
    public static final String FORWARD = "Forward";
    public static final String BACKWARD = "Backward";
    public static final String LEFT = "Left";
    public static final String RIGHT = "Right";
    public static final String PRIMARY = "Primary";
    public static final String SECONDARY = "Secondary";
    public static final String RELOAD = "Reload";
    public static final String SWITCH = "Switch";
    public static final String LOOK = "Look";
    public static final String RUN = "Run";
    private Camera camera;

    private Broadcaster networkConnection;
    private Vector3f EYE_LEVEL = new Vector3f(0, 4, 0);

    public PlayerControl(Broadcaster networkClient, Camera camera) {
        this.networkConnection = networkClient;
        this.camera = camera;
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        networkConnection.send(new PlayerInputMessage((int) spatial.getUserData(ID), name, isPressed));
    }

    @Override
    protected void controlUpdate(float tpf) {
        camera.setLocation(spatial.getLocalTranslation().add(EYE_LEVEL));
        networkConnection.send(new PlayerLookMessage((int) spatial.getUserData(ID), camera.getDirection()));
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

}
