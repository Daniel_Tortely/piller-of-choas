package explosions.client.entity;

import com.jme3.animation.AnimChannel;
import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Soverliss
 */
public class UpdateControl extends AbstractControl {

    private AnimChannel animChannel;
    private boolean dead = false;

    @Override
    protected void controlUpdate(float tpf) {
        if (dead) {
            boolean hasParticles = false;
            for (int i = 0; i < ((Node) spatial).getChildren().size(); i++) {
                if (((Node) spatial).getChild(i) instanceof ParticleEmitter) {
                    hasParticles = true;
                    ParticleEmitter pe = (ParticleEmitter) (((Node) spatial).getChild(i));
                    if (pe.getNumVisibleParticles() <= 0) {
                        spatial.removeFromParent();
                    }
                }
            }
            if (!hasParticles) {
                spatial.removeFromParent();
            }
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    public void move(Vector3f location) {
        spatial.setLocalTranslation(location);
    }

    public void setAnimChannel(AnimChannel animChannel) {
        this.animChannel = animChannel;
    }

    public void playAnimation(String name) {
        if ((animChannel.getAnimationName() == null) || (!animChannel.getAnimationName().equals(name))) {
            animChannel.setAnim(name);
        }
    }

    public void timesUp() {
        dead = true;
        for (int i = 0; i < ((Node) spatial).getChildren().size(); i++) {
            if (((Node) spatial).getChild(i) instanceof ParticleEmitter) {
                ParticleEmitter pe = (ParticleEmitter) (((Node) spatial).getChild(i));
                pe.setParticlesPerSec(0);
            }
        }
    }

}
