package explosions;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 * The main menu screen controller. Talks to the screen and the main class to
 * issue high level commands: start a server connect to a server as a client
 * quit the game
 *
 * @author Soverliss
 */
public class MenuScreen implements ScreenController {

    // The reference back to the main game class. Should really be using an interface here.
    private ExplosionsSometimes explosionsSometimes;
    private TextField serverPort; // reference to the GUI element
    private TextField clientIP;// reference to the GUI element
    private TextField clientPort;// reference to the GUI element
    private Label tutorial;

    /**
     * Create the screen controller and provide reference to main class
     * @param explosionsSometimes
     */
    public MenuScreen(ExplosionsSometimes explosionsSometimes) {
        this.explosionsSometimes = explosionsSometimes;
    }

    /**
     * Clicked the join button
     */
    public void onStartClient() {
        // extract the parameters from the GUI elements
        String ipAddress = clientIP.getRealText();
        int portNum = Integer.parseInt(clientPort.getRealText());
        // tell the main class to launch client
        explosionsSometimes.startClient(ipAddress, portNum);
    }

    /**
     * Clicked the start server button. Tell main class to start a server.
     */
    public void onStartServer() {
        explosionsSometimes.startServer(serverPort.getRealText());
    }

    /**
     * Clicked the quit button. Tell main class to exit.
     */
    public void onQuit() {
        explosionsSometimes.quit();
    }

    /**
     * The bind method is called by the architecture when the screen is loaded
     * and attached to this screen controller object. We use this opportunity to
     * find references to all the GUI elements defined by the XML so we can talk
     * to them through these variables in this class.
     *
     * @param nifty passed in by the architecture
     * @param screen passed in by the architecture
     */
    @Override
    public void bind(Nifty nifty, Screen screen) {
        serverPort = screen.findNiftyControl("serverPort", TextField.class);
        clientIP = screen.findNiftyControl("clientIP", TextField.class);
        clientPort = screen.findNiftyControl("clientPort", TextField.class);
        tutorial = screen.findNiftyControl("tutorial", Label.class);
    }

    /**
     * Unused but required because we're implementing an interface
     * Is automatically called once the screen is loaded.
     */
    @Override
    public void onStartScreen() {
    }

    /**
     * Unused but required because we're implementing an interface
     * Is automatically called when we exit the screen.
     */
    @Override
    public void onEndScreen() {
    }
}
