package explosions;

import com.jme3.app.SimpleApplication;
import com.jme3.network.serializing.Serializer;
import com.jme3.niftygui.NiftyJmeDisplay;
import de.lessvoid.nifty.Nifty;
import explosions.client.ClientAppState;
import explosions.client.HUDScreen;
import explosions.client.message.ClientMessage.ChatMessage;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import explosions.server.ServerAppState;
import explosions.server.message.ServerMessage.AnimationMessage;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.RemoveMessage;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.message.ServerMessage.UpdateHUDMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Top level class responsible only for creating the server and/or client
 * aspects of the game as appropriate (determined by the nifty menu)
 *
 * @author Soverliss
 */
public class ExplosionsSometimes extends SimpleApplication {

    private Nifty nifty; // The menuing system
    private HUDScreen hudScreen;
    @Override
    public void simpleInitApp() {
        // regardless of whether you choose to be the client and/or the server
        // in a given instance, network messages are ready to go
        registerNetworkMessages();
        // No dedicated mode (yet). Always show the menu
        initNifty();
        flyCam.setMoveSpeed(0);
    }

    /**
     * Register all the network message classes with the serializer so that they
     * can be sent across the network
     */
    private void registerNetworkMessages() {
        Serializer.registerClass(ChatMessage.class);
        Serializer.registerClass(PlayerInputMessage.class);
        Serializer.registerClass(PlayerLookMessage.class);
        Serializer.registerClass(UpdateLocationMessage.class);
        Serializer.registerClass(SpawnMessage.class);
        Serializer.registerClass(NetworkMessage.class);
        Serializer.registerClass(AudioMessage.class);
        Serializer.registerClass(AnimationMessage.class);
        Serializer.registerClass(RemoveMessage.class);
        Serializer.registerClass(UpdateHUDMessage.class);
    }

    /**
     * Load up the Nifty menu system.
     *
     * @throws SecurityException
     */
    private void initNifty() throws SecurityException {
        NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(
                assetManager, inputManager, audioRenderer, guiViewPort);
        nifty = niftyDisplay.getNifty();
        hudScreen = new HUDScreen();
        nifty.fromXml("Interface/screens.xml", "start", new MenuScreen(this),hudScreen);
        // Tell nifty not to log too much stuff in our output window
        Logger.getLogger("de.lessvoid.nifty").setLevel(Level.SEVERE);
        Logger.getLogger("NiftyInputEventHandlingLog").setLevel(Level.SEVERE);
        guiViewPort.addProcessor(niftyDisplay);
        flyCam.setDragToRotate(true);
    }

    /**
     * Create the server logic component of the game
     *
     * @param port the port number to run the server on
     */
    public void startServer(String port) {
        final ServerAppState serverAppState = new ServerAppState(port);
        stateManager.attach(serverAppState);
        // We must enqueue due to synchronisation between threads.
        // This will ensure that the server's startGame method isn't run
        // untill the next update method call, after the server state is initialised
        enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                // Hard-coded map to load
                serverAppState.startGame("Maps/map2.j3o");
                return null;
            }
        });
    }

    /**
     * Create the client logic component of the game
     *
     * @param ipAddress the network address to connect to
     */
    public void startClient(String ipAddress, int portNumber) {
        stateManager.attach(new ClientAppState(ipAddress, portNumber, hudScreen));
        nifty.gotoScreen("hud"); // this is lazy but it works
        flyCam.setDragToRotate(false); // remember to release the mouse from menu mode
    }

    /**
     * Terminate the game
     */
    public void quit() {
        this.stop();
    }
}
