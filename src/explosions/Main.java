package explosions;

/**
 *
 * @author Soverliss
 */
public class Main {

    /**
     * Main method just creates the game object and passes control to it.
     * Eventually we might want to parse command line parameters to, for
     * example, create a dedicated server mode.
     *
     * @param args
     */
    public static void main(String[] args) {
        ExplosionsSometimes app = new ExplosionsSometimes();
        app.start();
    }
}
