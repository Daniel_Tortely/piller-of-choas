/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package explosions.server;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Soverliss
 */
public class ForceControl extends AbstractControl {
    
    public static final float ROCKET_EXPLOSION_RADIUS = 1f;
    public static final float ROCKET_HIT_DAMAGE = 1f;
    public static final float ROCKET_EXPLOSION_DAMAGE = 50f;
    public static final float FORCE_COLLISION_RADIUS = 0.25f;
    public static final int ROCKET_VELOCITY = 75;
    public static final float FORCE_CONTAINER_MASS = 0.01f;

    public ForceControl() {
    }

    @Override
    protected void controlUpdate(float tpf) {
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

}
