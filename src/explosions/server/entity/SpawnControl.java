package explosions.server.entity;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Soverliss
 */
public class SpawnControl extends AbstractControl {

    private SpawnStrategy strategy;
    private float timer = Float.MAX_VALUE;
    private boolean gone;

    public SpawnControl(SpawnStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    protected void controlUpdate(float tpf) {
        if (gone) {
            timer += tpf;
        }
        if (timer > 5) {
            timer = 0;
            gone = false;
            strategy.spawn(spatial.getLocalTranslation(), this);
        }
    }

    public void spawnedTaken() {
        gone = true;
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

}
