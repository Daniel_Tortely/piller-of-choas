package explosions.server.entity;

import static explosions.client.entity.PlayerControl.BACKWARD;
import static explosions.client.entity.PlayerControl.FORWARD;
import static explosions.client.entity.PlayerControl.JUMP;
import static explosions.client.entity.PlayerControl.LEFT;
import static explosions.client.entity.PlayerControl.RIGHT;
import static explosions.client.entity.PlayerControl.PRIMARY;
import java.util.Random;

/**
 *
 * @author Soverliss
 */
public class BotControl extends PlayerControl {

    public BotControl(EntityUtility entityUtility) {
        super(entityUtility);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        if (new Random().nextFloat() > 0.98f) {
            switch (new Random().nextInt(12)) {
                case 0:
                    action(JUMP, true);
                    break;
                case 1:
                    action(JUMP, false);
                    break;
                case 2:
                    action(FORWARD, true);
                    break;
                case 3:
                    action(FORWARD, false);
                    break;
                case 4:
                    action(BACKWARD, true);
                    break;
                case 5:
                    action(BACKWARD, false);
                    break;
                case 6:
                    action(RIGHT, true);
                    break;
                case 7:
                    action(RIGHT, false);
                    break;
                case 8:
                    action(LEFT, true);
                    break;
                case 9:
                    action(LEFT, false);
                    break;
                case 10:
                    action(PRIMARY, true);
                    break;
                case 11:
                    action(PRIMARY, false);
                    break;
            }
        }
    }
}
