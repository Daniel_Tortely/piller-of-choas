package explosions.server.entity;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import explosions.server.message.ServerMessage.AnimationMessage;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import explosions.Broadcaster;
import static explosions.Factory.ID;
import explosions.server.message.ServerMessage.UpdateHUDMessage;

/**
 *
 * @author Soverliss
 */
public class UpdateControl extends AbstractControl {

    private Vector3f previous;
    private Broadcaster networkConnection;

    public UpdateControl(Broadcaster networkConnection) {
        this.networkConnection = networkConnection;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        previous = spatial.getLocalTranslation().clone();
// if we don't clone, previous is an alias and will only be the current location
    }

    @Override
    protected void controlUpdate(float tpf) {
        if (!previous.equals(spatial.getLocalTranslation())) {
            previous = spatial.getLocalTranslation().clone();
            networkConnection.send(new UpdateLocationMessage((int) spatial.getUserData(ID), spatial.getLocalTranslation()));
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    public void playSound(String name) {
        networkConnection.send(new AudioMessage((int) spatial.getUserData(ID), name));
    }

    public void playAnimation(String name) {
        networkConnection.send(new AnimationMessage((int) spatial.getUserData(ID), name));
    }

    public void updateHUD(String labelID, String info) {
        networkConnection.send(new UpdateHUDMessage((int) spatial.getUserData(ID), labelID, info), (int) spatial.getUserData(ID));
    }
}
