package explosions.server.entity;

import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;
import static explosions.client.HUDScreen.HEALTH;
import static explosions.client.HUDScreen.MANA;
import explosions.server.RayMessage;
import static explosions.client.entity.PlayerControl.BACKWARD;
import static explosions.client.entity.PlayerControl.FORWARD;
import static explosions.client.entity.PlayerControl.JUMP;
import static explosions.client.entity.PlayerControl.LEFT;
import static explosions.client.entity.PlayerControl.RELOAD;
import static explosions.client.entity.PlayerControl.RIGHT;
import static explosions.client.entity.PlayerControl.PRIMARY;
import static explosions.client.entity.PlayerControl.SECONDARY;
import static explosions.client.entity.PlayerControl.SWITCH;
import explosions.server.weapons.Mana;
import explosions.server.weapons.Weapon;
import java.util.ArrayList;
import java.util.List;
import static explosions.client.entity.PlayerControl.RUN;

/**
 *
 * @author Soverliss
 */
public abstract class PlayerControl extends BetterCharacterControl {

    private static final float PLAYER_SPEED = 10;
    private static final float RUN_RATIO = 2f;
    private static float walkSpeed = PLAYER_SPEED;
    private final float spellTime = 0.25f;
    private float spellTimer;
    private boolean doJump;
    private boolean goForward;
    private boolean goBackward;
    private boolean doPrimary;
    private boolean doSecondary;
    private boolean doReload;
    private boolean doSwitch;
    private boolean goRight;
    private boolean goLeft;
    private Weapon currentWeapon;
    private List<Weapon> weapons;
    public Mana mana;
    public int health;
    private EntityUtility entityUtil;
    private UpdateControl updateControl;

    public PlayerControl(EntityUtility entityUtil) {
        super(1, 4, 1);
        this.entityUtil = entityUtil;
        weapons = new ArrayList<>();
        mana = new Mana(100);
        health = 100;
        setJumpForce(new Vector3f(0, mass * 6, 0));
        spellTimer = 0;
    }

    public void setUpdateControl(UpdateControl updateControl) {
        this.updateControl = updateControl;
    }

    public void hurt(int amount) {
        health -= amount;
        if (health <= 0) {
            entityUtil.die(spatial);
        }
        updateControl.updateHUD(HEALTH, Integer.toString(health));
    }

    public void addHealth(int amount) {
        health += amount;
    }

    public Mana getMana() {
        return mana;
    }

    public void giveWeapon(Weapon weapon) {
        if (weapons.isEmpty()) {
            currentWeapon = weapon;
        }
        weapons.add(weapon);
    }

    public void action(String command, boolean pressed) {
        switch (command) {
            case JUMP:
                doJump = pressed;
                break;
            case FORWARD:
                goForward = pressed;
                break;
            case BACKWARD:
                goBackward = pressed;
                break;
            case LEFT:
                goLeft = pressed;
                break;
            case RIGHT:
                goRight = pressed;
                break;
            case PRIMARY:
                doPrimary = pressed;
                break;
            case SECONDARY:
                doSecondary = pressed;
                break;
            case RELOAD:
                doReload = pressed;
                break;
            case SWITCH:
                doSwitch = pressed;
                break;
            case RUN:
                if (pressed) {
                    walkSpeed = PLAYER_SPEED * RUN_RATIO;
                } else {
                    walkSpeed = PLAYER_SPEED;
                }
                break;
        }
    } 

    @Override
    public void update(float tpf) {
        super.update(tpf);
        currentWeapon.update(tpf);
        Vector3f direction = new Vector3f();
        if (doJump) {
            updateControl.playAnimation("Dodge");
            jump();
        }
        if (doPrimary) {
            currentWeapon.firePrimary();
            //updateControl.playSound("Sound/Effects/Gun.wav");
            updateControl.updateHUD(MANA, Integer.toString(mana.peekMana()));
        }
        if (doSecondary) {
            currentWeapon.fireSecondary();
            updateControl.playSound("Sound/Effects/Beep.ogg");
            updateControl.updateHUD(MANA, Integer.toString(mana.peekMana()));
        }
        if (doReload) {
            currentWeapon.reload();
            doReload = false;
        }
        if (doSwitch) {
            int index = weapons.indexOf(currentWeapon);
            index++;
            if (index > weapons.size() - 1) {
                index = 0;
            }
            currentWeapon = weapons.get(index);
            doSwitch = false;
        }

        if (goForward) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection);
        }
        if (goBackward) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection.negate());
        }
        if (goLeft) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection.cross(Vector3f.UNIT_Y.negate()));
        }
        if (goRight) {
            updateControl.playAnimation("Walk");
            direction.addLocal(viewDirection.cross(Vector3f.UNIT_Y));
        }
        
        if (!(goForward | goBackward | goLeft | goRight)) {
            updateControl.playAnimation("stand");
        }
        direction.normalizeLocal().multLocal(walkSpeed).multLocal(new Vector3f(1, 0, 1));
        setWalkDirection(direction);

    }

    public Vector3f getLocation() {
        return spatial.getLocalTranslation();
    }

    public float getHeight() {
        return getFinalHeight();
    }

    public void look(Vector3f lookDirection) {
        setViewDirection(lookDirection);
    }

    public void hit(RayMessage rayMessage) {
        hurt(rayMessage.getDamage());
    }

    public PhysicsCollisionObject getRigidBody() {
        return rigidBody;
    }

    public void givaMana(int i) {
        mana.giveMana(i);
    }
}
