package explosions.server.entity;

import com.jme3.scene.Spatial;

/**
 *
 * @author Soverliss
 */
public interface EntityUtility {

    void die(Spatial spatial);
}
