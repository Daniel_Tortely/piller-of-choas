/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package explosions.server.entity;

import com.jme3.math.Vector3f;
import explosions.server.ServerFactory;

/**
 *
 * @author Soverliss
 */
public abstract class SpawnStrategy {

    protected ServerFactory serverFactory;

    public SpawnStrategy(ServerFactory serverFactory) {
        this.serverFactory = serverFactory;
    }

    public abstract void spawn(Vector3f location, SpawnControl spawnControl);

    public static class AmmoStrategy extends SpawnStrategy {

        public AmmoStrategy(ServerFactory serverFactory) {
            super(serverFactory);
        }

        @Override
        public void spawn(Vector3f location, SpawnControl spawnControl) {
            serverFactory.makeMana(location, spawnControl);
        }

    }

}
