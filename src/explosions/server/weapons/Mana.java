package explosions.server.weapons;

/**
 *
 * @author Soverliss
 */
public class Mana {

    private int mana;

    public Mana(int mana) {
        this.mana = mana;
    }

    public int takeMana(int amount) {
        if (mana >= amount) {
            mana -= amount;
            return amount;
        } else {
            amount = mana;
            mana = 0;
            return amount;
        }
    }

    public int peekMana() {
        return mana;
    }

    public void giveMana(int i) {
        mana += i;
    }

    @Override
    public String toString() {
        return "Mana Pool: " + mana;
    }

}
