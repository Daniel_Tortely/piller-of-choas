package explosions.server.weapons;

import explosions.server.entity.PlayerControl;
import explosions.server.ServerFactory;
import explosions.server.weapons.Shooter;

/**
 *
 * @author Soverliss
 */
public class PistolShooter extends Shooter {

    public PistolShooter(ServerFactory serverFactory, PlayerControl playerControl) {
        super(serverFactory, playerControl);
    }

    @Override
    public void shoot() {
        serverFactory.makeSpell(playerControl.getLocation(), playerControl.getViewDirection(), playerControl.getRigidBody(), playerControl.getHeight() / 2);
    }

}
