package explosions.server.weapons;

/**
 *
 * @author Soverliss
 */
abstract public class Weapon {

    private enum WeaponState {
        READY, FIREPRIMARY, FIRESECONDARY, CHAMBER, RELOAD, RELOADING
    };
    private WeaponState state = WeaponState.READY;
    private final float refireTime;
    private float refireTimer;
    private final float reloadTime;
    private float reloadTimer;
    private int mana;
    private int loadMana;
    private final int manaCapacity;
    private final Mana manaPool;
    private final Shooter shooter;
    private String name;

    public Weapon(String name, Shooter shooter, float refireTime, float reloadTime, int mana, int manaCapacity, Mana manaPool) {
        this.name = name;
        this.refireTime = refireTime;
        this.reloadTime = reloadTime;
        this.mana = mana;
        this.manaCapacity = manaCapacity;
        this.manaPool = manaPool;
        this.shooter = shooter;
    }

    public void firePrimary() {
        if (state == WeaponState.READY) {
            state = WeaponState.FIREPRIMARY;
        }
    }

    public void fireSecondary() {
        if (state == WeaponState.READY) {
            state = WeaponState.FIRESECONDARY;
        }
    }

    public void reload() {
        if (state != WeaponState.RELOADING) {
            state = WeaponState.RELOAD;
        }
    }

    public void update(float tpf) {
        //System.out.println("Mana: " + mana);
        //System.out.println(manaPool);
        switch (state) {
            case FIREPRIMARY:
                if (mana > 0) {
                    mana--;
                    shooter.shoot();
                    state = WeaponState.CHAMBER;
                } else {
                    reload();
                }
                break;
            case FIRESECONDARY:
                if (mana > 0) {
                    mana--;
                    shooter.shoot();
                    state = WeaponState.CHAMBER;
                } else {
                    reload();
                }
                break;
            case CHAMBER:
                refireTimer += tpf;
                if (refireTimer >= refireTime) {
                    refireTimer = 0;
                    state = WeaponState.READY;
                }
                break;
            case RELOAD:
                if (mana < manaCapacity) {
                    if (manaPool.peekMana() > 0) {
                        state = WeaponState.RELOADING;
                        loadMana = manaPool.takeMana(manaCapacity - mana);
                    }
                } else {
                    state = WeaponState.READY;
                }
                break;
            case RELOADING:
                reloadTimer += tpf;
                if (reloadTimer >= reloadTime) {
                    reloadTimer = 0;
                    refireTimer = 0; // avoid rapid fire bug
                    mana += loadMana;
                    loadMana = 0;
                    state = WeaponState.READY;
                }
                break;
        }
    }
}
