package explosions.server.weapons;

import explosions.server.entity.PlayerControl;
import explosions.server.ServerFactory;

/**
 *
 * @author Soverliss
 */
public abstract class Shooter {

    protected ServerFactory serverFactory;
    protected PlayerControl playerControl;

    public Shooter(ServerFactory serverFactory, PlayerControl playerControl) {
        this.serverFactory = serverFactory;
        this.playerControl = playerControl;
    }

    public abstract void shoot();
}
