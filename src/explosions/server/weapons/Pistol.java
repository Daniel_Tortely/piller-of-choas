package explosions.server.weapons;

/**
 *
 * @author Soverliss
 */
public class Pistol extends Weapon {

    public Pistol(Shooter shooter, Mana cache) {
        super("Pistol", shooter, 0.5f, 2f, 3, 10, cache);
    }
}
