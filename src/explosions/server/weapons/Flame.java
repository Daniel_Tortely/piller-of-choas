package explosions.server.weapons;

/**
 *
 * @author Soverliss
 */
public class Flame extends Weapon {

    public Flame(Shooter shooter, Mana manaPool) {
        super("Flame", shooter, 0.01f, 0.01f, 1, 1, manaPool);
    }

}
