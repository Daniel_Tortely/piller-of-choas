package explosions.server.weapons;

import explosions.server.entity.PlayerControl;
import explosions.server.ServerFactory;
import explosions.server.weapons.Shooter;

/**
 *
 * @author Soverliss
 */
public class FlameShooter extends Shooter {

    public FlameShooter(ServerFactory serverFactory, PlayerControl playerControl) {
        super(serverFactory, playerControl);
    }

    @Override
    public void shoot() {
        serverFactory.makeFlameContainer(playerControl.getLocation(), playerControl.getViewDirection(), playerControl.getHeight() / 2);
    }
}
