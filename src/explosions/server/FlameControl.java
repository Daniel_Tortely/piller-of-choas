package explosions.server;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Soverliss
 */
public class FlameControl extends AbstractControl {

    public static final float ROCKET_EXPLOSION_RADIUS = 2f;
    public static final float ROCKET_HIT_DAMAGE = 1f;
    public static final float ROCKET_EXPLOSION_DAMAGE = 50f;
    public static final float FLAME_COLLISION_RADIUS = 0.25f;
    public static final int ROCKET_VELOCITY = 75;
    public static final float FLAME_CONTAINER_MASS = 0.01f;

    public FlameControl() {
    }

    @Override
    protected void controlUpdate(float tpf) {
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

}
