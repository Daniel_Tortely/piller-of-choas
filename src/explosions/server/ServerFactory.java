package explosions.server;

import explosions.server.entity.UpdateControl;
import explosions.server.entity.SpawnControl;
import explosions.server.entity.BotControl;
import explosions.server.entity.EntityUtility;
import explosions.server.entity.SpawnStrategy;
import explosions.server.entity.PlayerControl;
import explosions.server.entity.ItemControl;
import explosions.server.entity.HumanControl;
import explosions.server.weapons.FlameShooter;
import explosions.server.weapons.PistolShooter;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import explosions.Factory;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.weapons.Pistol;
import explosions.server.weapons.Flame;
import explosions.server.weapons.Shooter;
import explosions.Broadcaster;
import explosions.GameObjectFinder;
import static explosions.client.HUDScreen.ENEMIES;
import static explosions.server.FlameControl.ROCKET_VELOCITY;
import static explosions.server.FlameControl.FLAME_CONTAINER_MASS;
import static explosions.server.FlameControl.FLAME_COLLISION_RADIUS;
import static explosions.server.ForceControl.FORCE_COLLISION_RADIUS;
import static explosions.server.ForceControl.FORCE_CONTAINER_MASS;

/**
 *
 * @author Soverliss
 */
public class ServerFactory extends Factory {

    public static final String SOLID = "SOLID";
    private BulletAppState bulletAppState;
    private EntityUtility entityUtility;
    private WorldUtility worldUtility;
    private int entityId = 0;
    public int enemies = 0;
    private UpdateControl updateControl;

    public ServerFactory(WorldUtility worldUtility, EntityUtility entityUtility, BulletAppState bulletAppState, Broadcaster broadcaster, Node rootNode) {
        super(rootNode, broadcaster);
        this.bulletAppState = bulletAppState;
        this.entityUtility = entityUtility;
        this.worldUtility = worldUtility;
    }

    private RigidBodyControl givePhysics(float mass, Spatial spatial, CollisionShape collisionShape) {
        RigidBodyControl rigidBodyControl = null;
        if (collisionShape == null) {
            rigidBodyControl = new RigidBodyControl(mass);
        } else {
            rigidBodyControl = new RigidBodyControl(collisionShape, mass);
        }
        spatial.addControl(rigidBodyControl);
        bulletAppState.getPhysicsSpace().add(rigidBodyControl);
        return rigidBodyControl;
    }
    
    public void setUpdateControl(UpdateControl updateControl) {
        this.updateControl = updateControl;
    }

    public int makeWorld(Node worldRoot) {
        rootNode.attachChild(worldRoot);
        worldRoot.setUserData(ID, ++entityId);
        worldRoot.setName(MAP);
        for (Spatial component : ((Node) worldRoot.getChildren().get(0)).getChildren()) {
            final String name = component.getName().toUpperCase();
            if (name.contains(SOLID)) {
                givePhysics(0, component, null);
            } else if (name.contains(PLAYER_SPAWNER)) {
                //Bot Found Here
                makeBot(component.getLocalTranslation());
                enemies++;
            } else if (name.contains(MANA_SPAWNER)) {
                makeSpawner(component.getLocalTranslation(), new SpawnStrategy.AmmoStrategy(this));
            }
        }
        
        updateControl.updateHUD(ENEMIES, Integer.toString(enemies));
        
        for (Spatial spatial : GameObjectFinder.FindByType(worldRoot, Geometry.class)) {
            spatial.removeFromParent();
        }
        return entityId;
    }

    public int makeFlameContainer(Vector3f location, Vector3f direction, float spawnPoint) {
        Vector3f offset = direction.mult(5).add(0, spawnPoint + 1, 0);
        Node container = makeCore(location.add(offset), FLAME, ++entityId);
        RigidBodyControl rigidBodyControl = givePhysics(FLAME_CONTAINER_MASS, container, new SphereCollisionShape(FLAME_COLLISION_RADIUS));
        rigidBodyControl.setLinearVelocity(direction.mult(ROCKET_VELOCITY));
        rigidBodyControl.setGravity(new Vector3f(0,-10,0));
        container.addControl(new FlameControl());
        container.addControl(new UpdateControl(broadcaster));
        broadcaster.send(new SpawnMessage(entityId, FLAME, location));
        return entityId;
    }
    
    public int makeForceContainer(Vector3f location, Vector3f direction, float spawnPoint) {
        Vector3f offset = direction.mult(5).add(0, spawnPoint + 1, 0);
        Node container = makeCore(location.add(offset), FORCE, ++entityId);
        RigidBodyControl rigidBodyControl = givePhysics(FORCE_CONTAINER_MASS, container, new SphereCollisionShape(FORCE_COLLISION_RADIUS));
        rigidBodyControl.setLinearVelocity(direction.mult(ROCKET_VELOCITY));
        rigidBodyControl.setGravity(new Vector3f(0,-10,0));
        container.addControl(new FlameControl());
        container.addControl(new UpdateControl(broadcaster));
        broadcaster.send(new SpawnMessage(entityId, FORCE, location));
        return entityId;
    }

    public int makeHuman(Vector3f location) {
        final HumanControl humanControl = new HumanControl(entityUtility);
        giveSpells(humanControl);
        return makePlayer(location, humanControl);
    }

    public int makeBot(Vector3f location) {
        final BotControl botControl = new BotControl(entityUtility);
        giveSpells(botControl);
        return makePlayer(location, botControl);
    }

    private int makePlayer(Vector3f location, PlayerControl control) {
        Node container = makeCore(location, PLAYER, ++entityId);
        final UpdateControl updateControl = new UpdateControl(broadcaster);
        container.addControl(updateControl);
        control.setUpdateControl(updateControl);
        container.addControl(control);
        bulletAppState.getPhysicsSpace().add(container);
        return entityId;
    }

    private void giveSpells(PlayerControl control) {
        Shooter rocketShooter = new FlameShooter(this, control);
        Shooter bulletShooter = new PistolShooter(this, control);
        final Flame rocketLauncher = new Flame(rocketShooter, control.getMana());
        control.giveWeapon(rocketLauncher);
        final Pistol pistol = new Pistol(bulletShooter, control.getMana());
        control.giveWeapon(pistol);
    }

    public void makeSpell(Vector3f location, Vector3f viewDirection, PhysicsCollisionObject physicsCollisionObject, float spawnPoint) {
        location.addLocal(new Vector3f(0, spawnPoint, 0));
        worldUtility.rayCast(location, viewDirection, physicsCollisionObject, new RayMessage(100, 25, 500));
    }

    public void makeMana(Vector3f location, SpawnControl spawnControl) {
        Node container = makeCore(location, AMMO, ++entityId);
        givePhysics(0, container, new BoxCollisionShape(new Vector3f(1, 1, 1)));
        container.addControl(new UpdateControl(broadcaster));
        container.addControl(new ItemControl(spawnControl));
        broadcaster.send(new SpawnMessage(entityId, AMMO, location));
    }

    public void makeSpawner(Vector3f location, SpawnStrategy strategy) {
        Node container = makeCore(location, MANA_SPAWNER, ++entityId);
        container.addControl(new SpawnControl(strategy));
    }

}
