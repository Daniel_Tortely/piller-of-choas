package explosions.server;

/**
 * TODO: Name of class is too general
 * @author Soverliss
 */
public class RayMessage {

    private int penetration;
    private int damage;
    private int range;

    public RayMessage(int penetration, int damage, int range) {
        this.penetration = penetration;
        this.damage = damage;
        this.range = range;
    }

    public int getPenetration() {
        return penetration;
    }

    public int getRange() {
        return range;
    }

    public int getDamage() {
        return damage;
    }

}
