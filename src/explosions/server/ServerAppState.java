package explosions.server;

import explosions.server.entity.EntityUtility;
import explosions.server.entity.PlayerControl;
import explosions.server.entity.ItemControl;
import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.PhysicsRayTestResult;
import com.jme3.math.Vector3f;
import com.jme3.network.ConnectionListener;
import com.jme3.network.Filters;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import explosions.AppState;
import static explosions.Factory.ID;
import explosions.GameObjectFinder;
import explosions.NetworkMessage;
import explosions.client.message.ClientMessage.ChatMessage;
import explosions.client.message.ClientMessage.PlayerInputMessage;
import explosions.client.message.ClientMessage.PlayerLookMessage;
import explosions.server.message.ServerMessage.AudioMessage;
import explosions.server.message.ServerMessage.RemoveMessage;
import explosions.server.message.ServerMessage.SpawnMessage;
import explosions.server.message.ServerMessage.UpdateLocationMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import explosions.Broadcaster;
import explosions.Factory;
import static explosions.Factory.AMMO;
import static explosions.Factory.PLAYER;
import static explosions.Factory.MAP;
import static explosions.Factory.FLAME;
import static explosions.Factory.FORCE;
import java.util.Random;
import java.util.HashMap;

/**
 *
 * @author Soverliss
 */
public class ServerAppState extends AppState implements Broadcaster, PhysicsCollisionListener, EntityUtility, WorldUtility, ConnectionListener, MessageListener<HostedConnection> {

    private BulletAppState bulletAppState;
    private ServerFactory serverFactory;
    private Server myServer;
    private final String port;
    private HashMap<Integer, Integer> entityClientMap;

    public ServerAppState(String port) {
        this.port = port;
        entityClientMap = new HashMap<>();
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        bulletAppState.getPhysicsSpace().addCollisionListener(this);
        bulletAppState.setDebugEnabled(false);
        serverFactory = new ServerFactory(this, this, bulletAppState, this, rootNode);
        initNetwork();
    }

    @Override
    protected void initNetwork() {
        try {
            myServer = Network.createServer(Integer.parseInt(port));
            myServer.addMessageListener(this, NetworkMessage.class);
            myServer.addMessageListener(this, SpawnMessage.class);
            myServer.addMessageListener(this, ChatMessage.class);
            myServer.addMessageListener(this, PlayerInputMessage.class);
            myServer.addMessageListener(this, PlayerLookMessage.class);
            myServer.addMessageListener(this, UpdateLocationMessage.class);
            myServer.addConnectionListener(this);
            myServer.start();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public void startGame(String filePath) {
        serverFactory.makeWorld((Node) app.getAssetManager().loadModel(filePath));
    }

    /**
     * This is the method implemented by the PhysicsCollisionListener interface
     * which is automatically called by the physics engine any time two physics
     * objects touch
     *
     * @param event
     */
    @Override
    public void collision(PhysicsCollisionEvent event) {
        // We use our resolve method to see if either node A or node B involved in the collision
        Spatial player = resolve(PLAYER, event.getNodeA(), event.getNodeB());
        Spatial flame = resolve(FLAME, event.getNodeA(), event.getNodeB());
        Spatial force = resolve(FORCE, event.getNodeA(), event.getNodeB());
        Spatial ammo = resolve(AMMO, event.getNodeA(), event.getNodeB());

        // Once resolved, we can see which objects we are dealing with.
        if (flame != null) {
            if (player != null) {
                player.getControl(PlayerControl.class).hurt(2);
            }
            die(flame);
        }
        
        if (force != null) {
            if (player != null) {
                player.getControl(PlayerControl.class).hurt(2);
            }
            die(force);
        }

        if (ammo != null) {
            if (player != null) {
                player.getControl(PlayerControl.class).givaMana(25);
                ammo.getControl(ItemControl.class).take();
                die(ammo);
            }
        }

    }

    /**
     * A simple utility method which checks to see which of two nodes has the
     * name we're looking for
     *
     * @param name The name we're testing for
     * @param nodeA the first node involved in the collision
     * @param nodeB the second node involved in the collision
     * @return the node that had the name we were looking for
     */
    private Spatial resolve(String name, Spatial nodeA, Spatial nodeB) {
        if (nodeA.getName().equals(name)) {
            return nodeA;
        } else if (nodeB.getName().equals(name)) {
            return nodeB;
        }
        return null;
    }

    @Override
    public void connectionAdded(Server server, final HostedConnection conn) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                // find a spawn point in the world and pick one at random
                final Vector3f spawn = getSpawnPoint();
                int id = serverFactory.makeHuman(spawn);
                conn.send(new SpawnMessage(id, PLAYER, spawn, true));
                entityClientMap.put(id, conn.getId());

                myServer.broadcast(Filters.notIn(conn), new SpawnMessage(id, PLAYER, spawn));

                final HostedConnection guy = conn;

                rootNode.depthFirstTraversal(new SceneGraphVisitor() {
                    @Override
                    public void visit(Spatial spatial) {
                        boolean sendable = true;
                        if (spatial.getName() != null) {
                            String objectType = "";
                            switch (spatial.getName()) {
                                case MAP:
                                    objectType = MAP;
                                    break;
                                case FLAME:
                                    objectType = FLAME;
                                    break;
                                case FORCE:
                                    objectType = FORCE;
                                    break;
                                case PLAYER:
                                    objectType = PLAYER;
                                    break;
                                case AMMO:
                                    objectType = AMMO;
                                    break;
                                default:
                                    sendable = false;
                                    break;
                            }
                            if (sendable) {
                                guy.send(new SpawnMessage((int) spatial.getUserData(ID), objectType, spatial.getLocalTranslation()));
                            }
                        }
                    }
                });
                myServer.broadcast(new AudioMessage(id, "Sound/Effects/Bang.wav"));
                return null;
            }

        });

    }

    private Vector3f getSpawnPoint() {
        Vector3f location = new Vector3f();
        List<Spatial> spawnPoints = GameObjectFinder.FindByName(rootNode, Factory.PLAYER_SPAWNER);
        return spawnPoints.get(new Random().nextInt(spawnPoints.size())).getLocalTranslation();
    }

    @Override
    public void send(NetworkMessage networkMessage) {
        myServer.broadcast(networkMessage);
    }

    @Override
    public void send(NetworkMessage networkMessage, int entityId) {
        final Integer connectionId = entityClientMap.get(entityId);
        if (connectionId != null) {
            myServer.broadcast(Filters.in(myServer.getConnection(connectionId)), networkMessage);
        }
    }

    @Override
    public void messageReceived(HostedConnection source, final Message message) {
        app.enqueue(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (message instanceof ChatMessage) {
                    ChatMessage helloMessage = (ChatMessage) message;
                } else if (message instanceof PlayerInputMessage) {
                    PlayerInputMessage pim = (PlayerInputMessage) message;
                    handlePlayerInput(pim.id, pim.command, pim.pressed);
                } else if (message instanceof PlayerLookMessage) {
                    PlayerLookMessage plm = (PlayerLookMessage) message;
                    handlePlayerLook(plm.id, plm.lookDirection);
                }
                return null;
            }
        });

    }

    private void handlePlayerInput(final int id, final String command, final boolean pressed) {
        List<Spatial> spatials = GameObjectFinder.FindById(rootNode, id);
        if (spatials.size() > 0) {
            PlayerControl pc = spatials.get(0).getControl(PlayerControl.class);
            pc.action(command, pressed);
        }
    }

    private void handlePlayerLook(int id, Vector3f lookDirection) {
        List<Spatial> spatials = GameObjectFinder.FindById(rootNode, id);
        if (spatials.size() > 0) {
            PlayerControl pc = spatials.get(0).getControl(PlayerControl.class);
            pc.look(lookDirection);
        }
    }

    @Override
    public void rayCast(Vector3f from, Vector3f to, PhysicsCollisionObject sender, RayMessage rayMessage) {
        to = from.add(to.mult(rayMessage.getRange()));
        List<PhysicsRayTestResult> results = new ArrayList<>();
        bulletAppState.getPhysicsSpace().rayTest(from, to, results);
        for (int i = results.size() - 1; i >= 0; i--) {
            if (i < rayMessage.getPenetration()) {
                if (results.get(i).getCollisionObject().getUserObject() != null) {
                    rayHit((Spatial) results.get(i).getCollisionObject().getUserObject(), rayMessage);
                }
            }
        }
    }

    private void rayHit(Spatial spatial, RayMessage rayMessage) {
        PlayerControl pc = spatial.getControl(PlayerControl.class);
        if (pc != null) {
            pc.hit(rayMessage);
        }
    }

    @Override
    public void die(Spatial spatial) {
        send(new RemoveMessage((int) spatial.getUserData(ID)));
        bulletAppState.getPhysicsSpace().removeAll(spatial);
        spatial.removeFromParent();
    }

    @Override
    public void connectionRemoved(Server server, HostedConnection conn) {
    }

    @Override
    public void cleanup() {
        super.cleanup();
        myServer.close();
    }

}
