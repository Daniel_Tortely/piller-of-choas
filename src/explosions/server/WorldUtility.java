package explosions.server;

import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.math.Vector3f;

/**
 *
 * @author Soverliss
 */
interface WorldUtility {

    void rayCast(Vector3f to, Vector3f from, PhysicsCollisionObject sender, RayMessage rayMessage);
}
