package explosions;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

/**
 * Factory base class controls the construction of game objects.
 * @author Soverliss
 */
public abstract class Factory {

    // We need to be able to identify unique game objects
    public static final String ID = "ID";
    public static final String AMMO = "Ammo";
    public static final String MAP = "Map";
    public static final String PLAYER = "Player";
    public static final String PLAYER_SPAWNER = "PLAYER_SPAWN";
    public static final String FLAME = "Flame";
    public static final String FORCE = "Force";
    public static final String MANA_SPAWNER = "AMMO_SPAWN";
    // The rootnode will be unique for client or server side
    // this is the attachment point for adding game objects
    protected Node rootNode;
    // Handed to game objects which need to send updates directly through the network
    protected Broadcaster broadcaster;

    public Factory(Node rootNode, Broadcaster broadcaster) {
        this.rootNode = rootNode;
        this.broadcaster = broadcaster;
    }

    /**
     * Utility function used by both server and client to generate the basic
     * components of every game object
     * @param location where the object is to be placed
     * @param name the unique identifier specifying what kind of object this is
     * @param id the unique id used to refer to a specific game object
     * @return the completed shell 
     */
    protected Node makeCore(Vector3f location, String name, int id) {
        Node node = new Node(name);
        node.setLocalTranslation(location);
        rootNode.attachChild(node);
        node.setUserData(ID, id);
        return node;
    }
}
