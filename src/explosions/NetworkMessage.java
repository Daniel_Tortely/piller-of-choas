package explosions;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 * The base class for all network messaging classes.
 *
 * @author Soverliss
 */
@Serializable
public abstract class NetworkMessage extends AbstractMessage {

    /**
     * Serialisation constructor
     */
    public NetworkMessage() {
    }
}
